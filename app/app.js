import React from 'react';
import { View, Component, AsyncStorage} from 'react-native';
import {
  createSwitchNavigator,
  createStackNavigator,
  createBottomTabNavigator,
  createAppContainer
} from 'react-navigation';
import { withRkTheme } from 'react-native-ui-kitten';
import { AppRoutes } from './config/navigation/routesBuilder';
import * as Screens from './screens';
import { bootstrap } from './config/bootstrap';
//import track from './config/analytics';
import { data } from './data';
import Icon from 'react-native-vector-icons/Ionicons';
import {LoginV2} from './screens/login/login2';
import {SignUp} from './screens/login/signUp';
import {AuthLoadingScreen} from './screens/loading/AuthLoading'


bootstrap();
data.populateData();

const AppStack = createBottomTabNavigator(
  {
    ...AppRoutes,
  },
  {
    tabBarOptions: {
      showLabel:false,
    }
  }
)

const AuthStack = createStackNavigator({ Login: LoginV2 , SignUp: SignUp});

const KittenApp = createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    App: AppStack,
    Auth: AuthStack,
  },
  {
    initialRouteName: 'AuthLoading',
  }
);

export default class App extends React.Component {

  constructor() {
    super();
    this.state = { isLoaded: false };
  }

  onNavigationStateChange = (previous, current) => {
    const screen = {
      current: this.getCurrentRouteName(current),
      previous: this.getCurrentRouteName(previous),
    };
  };

  getCurrentRouteName = (navigation) => {
    const route = navigation.routes[navigation.index];
    return route.routes ? this.getCurrentRouteName(route) : route.routeName;
  };

  loadAssets = async () => {
    
  };

  renderLogin = () => (
    <View style={{ flex: 1 }}>
      <LoginV2/>
    </View>
  );

  renderApp = () => (
    <View style={{ flex: 1 }}>
      <KittenApp onNavigationStateChange={this.onNavigationStateChange} />
    </View>
  );

  render = () => (this.renderApp());
}

