import _ from 'lodash';
import { FontIcons, FontAwesome } from '../../assets/icons';
import * as Screens from '../../screens/index';
import Icon from 'react-native-vector-icons/FontAwesome';
import React from 'react';
import ReactNative from 'react-native';

const CustomTabBarIcon = (name, size) => {
  const icon = () => (
    <Icon
      name='ios-search'
      size={size}
      color='#000000'
    />
  );
}

export const MainRoutes = [
  {
    id: 'Feed',
    screen: Screens.Blogposts,
    icon: FontAwesome.heart,
    children: [
      {
        id: 'Article',
        title: '',
        screen: Screens.Article,
        children:[],
      },
      {
        id: 'Comments',
        title: '',
        screen: Screens.Comments,
        children:[],
      },
    ],
  },
  {
    id: 'Explore',
    title: '',
    icon: CustomTabBarIcon("ios-search",22),
    screen: Screens.Contacts,
    children:[
    ],
  },
  {
    id: 'Notifications',
    title: '',
    icon: CustomTabBarIcon("ios-notifications",22),
    screen: Screens.Notifications,
    children:[],
  },
  {
    id: 'Profile',
    title: '',
    icon:CustomTabBarIcon("user-circle",30),
    screen: Screens.ProfileV2,
    children: [
      {
        id: 'ProfileV1',
        title: '',
        screen: Screens.ProfileV1,
        children:[],
      },
      {
        id: 'ChatList',
        title: '',
        screen: Screens.ChatList,
        children:[],
      },
      {
        id: 'Chat',
        title: '',
        screen: Screens.Chat,
        children:[],
      }
    ],
  },
];

const menuRoutes = _.cloneDeep(MainRoutes);

export const MenuRoutes = menuRoutes;
