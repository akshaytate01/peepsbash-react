import React from 'react';
import _ from 'lodash';
import { createStackNavigator } from 'react-navigation';
import { withRkTheme } from 'react-native-ui-kitten';
import { NavBar } from '../../components/index';
import transition from './transitions';
import Icon from 'react-native-vector-icons/FontAwesome'
import {
  MainRoutes,
  MenuRoutes,
} from './routes';
import { FontAwesome ,FontIcons} from '../../assets/icons';
import {Image} from 'react-native';

const main = {};
const flatRoutes = {};

const routeMapping = (route) => ({
  screen: withRkTheme(route.screen),
  title: route.title,
});

(MenuRoutes).forEach(route => {
  flatRoutes[route.id] = routeMapping(route);
  main[route.id] = routeMapping(route);
  route.children.forEach(nestedRoute => {
    flatRoutes[nestedRoute.id] = routeMapping(nestedRoute);
  });
});

const renderHeader = (navigation, props) => {
  const ThemedNavigationBar = withRkTheme(NavBar);
  return (
    <ThemedNavigationBar navigation={navigation} headerProps={props} />
  );
};

const DrawerRoutes = Object.keys(main).reduce((routes, name) => {
  const rawRoutes = routes;
  rawRoutes[name] = {
    name,
    screen: createStackNavigator(flatRoutes, {
      initialRouteName: name,
      headerMode: 'screen',
      cardStyle: { backgroundColor: 'transparent' },
      transitionConfig: transition,
      

    }),
    navigationOptions: {
      //tabBarLabel: 'Feed',
      tabBarIcon: ({focused}) => {
        if(name == 'Feed'){
          if(!{focused})
            return <Image source={require('../../assets/icons/explore.jpeg')} style={{width: 25, height: 25}}/>
          else
            return <Image source={require('../../assets/icons/feed.jpeg')} style={{width: 25, height: 25}}/>
        }
        else if(name == 'Explore')
          return <Image source={require('../../assets/icons/explore.jpeg')} style={{width: 25, height: 25}}/>
        else if(name == 'Notifications')
          return <Image source={require('../../assets/icons/notifications.jpeg')} style={{width: 25, height: 25}}/>
        else if(name == 'Profile')
          return <Image source={require('../../assets/icons/profile.jpeg')} style={{width: 25, height: 25}}/>         
        },
    },
  };
  return rawRoutes;
}, {});

export const AppRoutes = DrawerRoutes;
export const FeedRoute = _.find(MainRoutes, { id: 'Feed' }).children;
export const ExploreRoute = _.find(MainRoutes, { id: 'Explore' }).children;
export const NotificationRoute = _.find(MainRoutes, { id: 'Notifications' }).children;
export const Profile = _.find(MainRoutes, { id: 'Profile' }).children;
